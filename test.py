def map_major_pentatonic(lnotes):
  scale_map = np.array([0, 0, 2, 2, 4, 4, 4, 7, 7, 9, 9, 9])
  pause_mask = lnotes == -1
  octave = lnotes // 12
  notes = lnotes % 12
  mapped_scale = scale_map[notes] + 12 * octave
  mapped_scale[pause_mask] = -1
  return mapped_scale

def map_minor_pentatonic(lnotes):
  scale_map = np.array([0, 0, 0, 3, 3, 5, 5, 7, 7, 7, 10, 10])
  pause_mask = lnotes == -1
  octave = lnotes // 12
  notes = lnotes % 12
  mapped_scale = scale_map[notes] + 12 * octave
  mapped_scale[pause_mask] = -1
  return mapped_scale

def map_harmonic_minor(lnotes):
  scale_map = np.array([0, 0, 2, 3, 3, 5, 5, 7, 8, 8, 8, 11])
  pause_mask = lnotes == -1
  octave = lnotes // 12
  notes = lnotes % 12
  mapped_scale = scale_map[notes] + 12 * octave
  mapped_scale[pause_mask] = -1
  return mapped_scale

def map_phrygian(lnotes):
  scale_map = np.array([0, 1, 1, 3, 3, 5, 5, 7, 8, 8, 10, 10])
  pause_mask = lnotes == -1
  octave = lnotes // 12
  notes = lnotes % 12
  mapped_scale = scale_map[notes] + 12 * octave
  mapped_scale[pause_mask] = -1
  return mapped_scale


def map_flamenco(lnotes):
  scale_map = np.array([0, 1, 1, 3, 3, 5, 6, 6, 8, 9, 10, 10])
  pause_mask = lnotes == -1
  octave = lnotes // 12
  notes = lnotes % 12
  mapped_scale = scale_map[notes] + 12 * octave
  mapped_scale[pause_mask] = -1
  return mapped_scale



def map_blues(lnotes):
  scale_map = np.array([0, 0, 0, 3, 3, 5, 6, 7, 8, 9, 10, 10])
  pause_mask = lnotes == -1
  octave = lnotes // 12
  notes = lnotes % 12
  mapped_scale = scale_map[notes] + 12 * octave
  mapped_scale[pause_mask] = -1
  return mapped_scale


def map_minor(lnotes):
  scale_map = np.array([0, 0, 2, 3, 3, 5, 5, 7, 8, 8, 10, 10])
  pause_mask = lnotes == -1
  octave = lnotes // 12
  notes = lnotes % 12
  mapped_scale = scale_map[notes] + 12 * octave
  mapped_scale[pause_mask] = -1
  return mapped_scale

def map_major(lnotes):
  scale_map = np.array([0, 0, 2, 2, 4, 5, 5, 7, 7, 9, 9, 11])
  pause_mask = lnotes == -1
  octave = lnotes // 12
  notes = lnotes % 12
  mapped_scale = scale_map[notes] + 12 * octave
  mapped_scale[pause_mask] = -1
  return mapped_scale
  

def getChromosomeSizesFromBigWig(bwname):
  csize = {}
  fh = open(os.path.expanduser(bwname), "rb")
  magic = fh.read(4)
  if magic == '&\xfc\x8f\x88':
    endianness = '<'
  elif magic == '\x88\x8f\xfc&':
    endianness = '>'
  else:
    raise IOError("The file is not in bigwig format")
  (version,zoomLevels,chromosomeTreeOffset,fullDataOffset,fullIndexOffset,fieldCount,definedFieldCount,autoSqlOffset,totalSummaryOffset,uncompressBufSize,reserved)=struct.unpack(endianness+'HHQQQHHQQIQ',fh.read(60))
  if version < 3:
    raise IOError("Bigwig files version <3 are not supported")
  fh.seek(chromosomeTreeOffset)
  magic = fh.read(4)
  if magic == '\x91\x8c\xcax':
    endianness = '<'
  elif magic == 'x\xca\x8c\x91':
    endianness = '>'
  else:
    raise ValueError("Wrong magic for this bigwig data file")
  (blockSize, keySize, valSize, itemCount, reserved) = struct.unpack(endianness + 'IIIQQ', fh.read(28))
  (isLeaf, reserved, count) = struct.unpack(endianness + 'BBH', fh.read(4))
  for n in range(count):
    (key, chromId, chromSize) = struct.unpack(endianness + str(keySize) + 'sII', fh.read(keySize + 2 * 4))
    csize[key.replace('\x00', '')] = chromSize
  return csize

def build_octave(interval, n):
  return np.cumsum(np.ones(12 * n) * interval) + np.log1p(1) - interval

def build_track(file_name, chrom, start, end, scale = 'chromatic', tune = 48, tick_size = 100, genome_bin = 200):
  bh = bw.BigWigFile(open(file_name, 'rb'))
  chrom_sizes = getChromosomeSizesFromBigWig(file_name)
  signal =  bh.get_as_array(chrom, start, end)
  signal[np.isnan(signal)] = 0
  ss  = bh.summarize(chrom, 1, chrom_sizes[chrom], 1000)
  lsmeans = np.log1p(np.mean(np.log1p(signal)[:, np.newaxis].reshape((len(signal)/genome_bin, genome_bin)), axis=1))
  #here add padding, in case
#  lbins = np.linspace(np.log1p(1), np.log1p(np.median(ss.max_val)), 12*4)
  notediff = (np.log1p(np.median(ss.max_val)) -  np.log1p(1)) / 48
  octaves = build_octave(notediff, 6)
  lnotes  = np.sum(lsmeans > octaves[:, np.newaxis], axis=0) - 1 
  
  if scale == 'major':
    lnotes = map_major(lnotes)
  elif scale == 'minor':
    lnotes = map_minor(lnotes)  
  elif scale == 'blues':
    lnotes = map_blues(lnotes)  
  elif scale == 'flamenco':
    lnotes = map_flamenco(lnotes)  
  elif scale == 'phrygian':
    lnotes = map_phrygian(lnotes)  
  elif scale == 'harmonic_minor':
    lnotes = map_harmonic_minor(lnotes)  
  elif scale == 'major_pentatonic':
    lnotes = map_major_pentatonic(lnotes)  
  elif scale == 'minor_pentatonic':
    lnotes = map_minor_pentatonic(lnotes)  

  midi_data = []
  last_note = lnotes[0]
  note_length = 1
  for x in range(1, len(lnotes)):
    this_note = lnotes[x]
    if this_note == last_note:
      note_length += 1
    else:
      midi_data.append([note_length * tick_size, 64, last_note])
      last_note = this_note
      note_length = 1

  pattern = midi.Pattern()
  track = midi.Track()
  pattern.append(track)

  tick = 0
  for x in midi_data:
    if x[2] > -1:
      on = midi.NoteOnEvent(tick = tick, velocity = x[1], pitch = x[2] + tune)
      track.append(on)
      tick = 0
      off = midi.NoteOffEvent(tick = tick + x[0], pitch = x[2] + tune)
      track.append(off)
    else:
      tick = x[0]

  track.append(midi.EndOfTrackEvent(tick=1))
  return pattern

pattern = build_track("K562_H3K27ac.bigWig", 'chr12', r[1] - 20000, r[1] + 80000, tick_size = 200, scale = 'blues', tune = 49)
midi.write_midifile("k27ac.mid", pattern)
pattern = build_track("K562_H3K27me3.bigWig", 'chr12', r[1] - 20000, r[1] + 80000, tick_size = 200, scale = 'blues', tune = 25)
midi.write_midifile("k27me3.mid", pattern)
pattern = build_track("K562_H3K4me3.bigWig", 'chr12', r[1] - 20000, r[1] + 80000, tick_size = 200, scale = 'blues', tune = 49)
midi.write_midifile("k4me3.mid", pattern)
pattern = build_track("K562_H3K9ac.bigWig", 'chr12', r[1] - 20000, r[1] + 80000, tick_size = 200, scale = 'blues', tune = 61)
midi.write_midifile("k9ac.mid", pattern)
pattern = build_track("K562_H3K9me3.bigWig", 'chr12', r[1] - 20000, r[1] + 80000, tick_size = 200, scale = 'blues', tune = 25)
midi.write_midifile("k9me3.mid", pattern)
pattern = build_track("K562_RNAPIIS5P.bigWig", 'chr12', r[1] - 20000, r[1] + 80000, tick_size = 200, scale = 'blues', tune = 61)
midi.write_midifile("rnap5p.mid", pattern)


pattern = build_track("K562_H3K27ac.bigWig", 'chr19', 100000, 5100000, tick_size = 300, scale = 'major', tune = midi.G_4, genome_bin=50000)
midi.write_midifile("k27ac.mid", pattern)
pattern = build_track("K562_H3K27me3.bigWig", 'chr19', 100000, 5100000, tick_size = 300, scale = 'major', tune = midi.G_5, genome_bin=50000)
midi.write_midifile("k27me3.mid", pattern)
pattern = build_track("K562_H3K4me3.bigWig", 'chr19', 100000, 5100000, tick_size = 300, scale = 'major', tune = midi.G_4, genome_bin=50000)
midi.write_midifile("k4me3.mid", pattern)
pattern = build_track("K562_H3K9ac.bigWig", 'chr19', 100000, 5100000, tick_size = 300, scale = 'major', tune = midi.G_4, genome_bin=50000)
midi.write_midifile("k9ac.mid", pattern)
pattern = build_track("K562_H3K9me3.bigWig", 'chr19', 100000, 5100000, tick_size = 300, scale = 'major', tune = midi.G_5, genome_bin=50000)
midi.write_midifile("k9me3.mid", pattern)
pattern = build_track("K562_RNAPIIS5P.bigWig", 'chr19', 100000, 5100000, tick_size = 300, scale = 'major', tune = midi.G_4, genome_bin=50000)
midi.write_midifile("rnap5p.mid", pattern)


pattern = build_track("K562_H3K27ac.bigWig", r[0], r[1] - 50000, r[1] + 250000, tick_size = 100, scale = 'major_pentatonic', tune = midi.C_4)
midi.write_midifile("k27ac.mid", pattern)
pattern = build_track("K562_H3K27me3.bigWig", r[0], r[1] - 50000, r[1] + 250000, tick_size = 100, scale = 'major_pentatonic', tune = midi.G_4)
midi.write_midifile("k27me3.mid", pattern)
pattern = build_track("K562_H3K4me3.bigWig", r[0], r[1] - 50000, r[1] + 250000, tick_size = 100, scale = 'minor_pentatonic', tune = midi.E_4)
midi.write_midifile("k4me3.mid", pattern)
pattern = build_track("K562_H3K9ac.bigWig", r[0], r[1] - 50000, r[1] + 250000, tick_size = 100, scale = 'major_pentatonic', tune = midi.C_5)
midi.write_midifile("k9ac.mid", pattern)
pattern = build_track("K562_H3K9me3.bigWig", r[0], r[1] - 50000, r[1] + 250000, tick_size = 100, scale = 'major_pentatonic', tune = midi.C_4)
midi.write_midifile("k9me3.mid", pattern)
pattern = build_track("K562_RNAPIIS5P.bigWig", r[0], r[1] - 50000, r[1] + 250000, tick_size = 100, scale = 'major_pentatonic', tune = midi.G_5)
midi.write_midifile("rnap5p.mid", pattern)


pattern = build_track("K562_H3K27ac.bigWig", r[0], r[1] - 50000, r[1] + 1500000, tick_size = 100, scale = 'minor_pentatonic', tune = midi.D_4, genome_bin=500)
midi.write_midifile("k27ac.mid", pattern)
pattern = build_track("K562_H3K27me3.bigWig", r[0], r[1] - 50000, r[1] + 1500000, tick_size = 100, scale = 'minor_pentatonic', tune = midi.D_3, genome_bin=500)
midi.write_midifile("k27me3.mid", pattern)
pattern = build_track("K562_H3K4me3.bigWig", r[0], r[1] - 50000, r[1] + 1500000, tick_size = 100, scale = 'minor_pentatonic', tune = midi.D_4, genome_bin=500)
midi.write_midifile("k4me3.mid", pattern)
pattern = build_track("K562_H3K9ac.bigWig", r[0], r[1] - 50000, r[1] + 1500000, tick_size = 100, scale = 'minor_pentatonic', tune = midi.D_5, genome_bin=500)
midi.write_midifile("k9ac.mid", pattern)
pattern = build_track("K562_H3K9me3.bigWig", r[0], r[1] - 50000, r[1] + 1500000, tick_size = 100, scale = 'chromatic', tune = midi.D_4, genome_bin=500)
midi.write_midifile("k9me3.mid", pattern)
pattern = build_track("K562_RNAPIIS5P.bigWig", r[0], r[1] - 50000, r[1] + 1500000, tick_size = 100, scale = 'minor_pentatonic', tune = midi.D_6, genome_bin=500)
midi.write_midifile("rnap5p.mid", pattern)

#!/usr/bin/env python2.7

import os
import midi
import argparse
import sys


def merge_MIDI_tracks():


  # parse options
  option_parser = argparse.ArgumentParser(
  description="Dummy merger for mutliple MIDI files",
  prog="MIDI_merge.py",
  epilog="For any question, write to cittaro.davide@hsr.it")
  option_parser.add_argument("--version", action="version", version="%(prog)s 0.1")
  option_parser.add_argument("-o", "--output", help="Basename of output MIDI file", action='store', default='pattern')
  option_parser.add_argument("midi_files", nargs="+")

  options = option_parser.parse_args()
  
  if not options.output.endswith('.mid'):
    options.output = options.output + '.mid'

  pattern_list = []
  for x in options.midi_files:
    pattern_list.append(midi.read_midifile(x))
  
  _resolution = max([x.resolution for x in pattern_list])

  out_pattern = midi.Pattern(resolution = _resolution)
  
  for pattern in pattern_list:
    for track in pattern:
      out_pattern.append(track)

  midi.write_midifile(options.output, out_pattern)

if __name__ == '__main__':
  merge_MIDI_tracks()

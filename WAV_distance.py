#!/usr/bin/env python
from features import mfcc
import scipy.io.wavfile as wav
from sklearn.mixture import GMM
import numpy as np
import sys



def hausdorff_distance(s1, s2):
  mx = []
  my = []
  for x in s1:
    d = min([np.linalg.norm(x - y) for y in s2])
    mx.append(d)
  for y in s2:
    d = min([np.linalg.norm(y - x) for x in s1])
    my.append(d)
  return max(mx + my)        

gmm_means = []
dm = []

id_list = []
for line in open("ids"):
  id_list.append(line.strip())

cep_n = int(sys.argv[1])
gmm_n = int(sys.argv[2])


for region_id in id_list:
  wav_list = ["/lustre2/scratch/Cittaro/ENCONCERT/wav/%s/%s_%s.wav" % (x, x, region_id) for x in ['K562', 'K562_2', 'NHEK', 'NHEK_2']]
  signal_list = []
  gmm_model = GMM(gmm_n, n_iter=100, covariance_type='full')
  gmm_means = []
  dm = []
  for wav_file in wav_list:
    (rate, sig) = wav.read(wav_file)
    mfcc_feat = mfcc(sig, rate, numcep=cep_n)
    gmm_model.fit(mfcc_feat)
    gmm_means.append(gmm_model.means_)
  l = len(gmm_means)
  if l > 1:
    for gx in range(l - 1):
      for gy in range(gx + 1, l):
        d = hausdorff_distance(gmm_means[gx], gmm_means[gy])
        dm.append(d)
  else:
    dm = np.zeros(6) + np.nan     
  sys.stdout.write("%s\t%d\t%d\t%s\n" % (region_id, cep_n, gmm_n, '\t'.join(["%.4e" % dx for dx in dm])))
  sys.stdout.flush()

#!/usr/bin/env python2.7

import numpy as np
import os
import struct
import midi
import bx.bbi.bigwig_file as bigwig
import argparse
import sys
import pickle

_resolution = 960 #default resolution for MIDI pattern in python-midi
_mapped_octaves = 6
#_mapped_octaves = 4


def map_scale(lnotes, scale_map = np.arange(12)):
  # default scale is no scale, aka chromatic
  pause_mask = lnotes == -1
  octave = lnotes // 12
  notes = lnotes % 12
  mapped_scale = scale_map[notes] + 12 * octave
  mapped_scale[pause_mask] = -1
  return mapped_scale

def getChromosomeSizesFromBigWig(bwname):
  csize = {}
  fh = open(os.path.expanduser(bwname), "rb")
  magic = fh.read(4)
  if magic == '&\xfc\x8f\x88':
    endianness = '<'
  elif magic == '\x88\x8f\xfc&':
    endianness = '>'
  else:
    raise IOError("The file is not in bigwig format")
  (version,zoomLevels,chromosomeTreeOffset,fullDataOffset,fullIndexOffset,fieldCount,definedFieldCount,autoSqlOffset,totalSummaryOffset,uncompressBufSize,reserved)=struct.unpack(endianness+'HHQQQHHQQIQ',fh.read(60))
  if version < 3:
    raise IOError("Bigwig files version <3 are not supported")
  fh.seek(chromosomeTreeOffset)
  magic = fh.read(4)
  if magic == '\x91\x8c\xcax':
    endianness = '<'
  elif magic == 'x\xca\x8c\x91':
    endianness = '>'
  else:
    raise ValueError("Wrong magic for this bigwig data file")
  (blockSize, keySize, valSize, itemCount, reserved) = struct.unpack(endianness + 'IIIQQ', fh.read(28))
  (isLeaf, reserved, count) = struct.unpack(endianness + 'BBH', fh.read(4))
  for n in range(count):
    (key, chromId, chromSize) = struct.unpack(endianness + str(keySize) + 'sII', fh.read(keySize + 2 * 4))
    csize[key.replace('\x00', '')] = chromSize
  return csize

def build_octave(interval, n):
  return np.cumsum(np.ones(12 * n) * interval) + np.log1p(1) - interval


def build_track(trackin_file_name, chrom, start, end, scale = 'chromatic', tune = 48, tick_size = 100, genome_bin = 200, velocity_file_name = None):
  bh = bigwig.BigWigFile(open(trackin_file_name, 'rb'))
  chrom_sizes = getChromosomeSizesFromBigWig(trackin_file_name)
  signal =  bh.get_as_array(chrom, start, end)
  signal[np.isnan(signal)] = 0
#  np.random.shuffle(signal) #random signal
  ss  = bh.summarize(chrom, 1, chrom_sizes[chrom], 1000)
  
  # pad zeros to the end, just in case
  signal = np.append(signal, np.zeros(genome_bin - (len(signal) % genome_bin)))
  
  lsmeans = np.log1p(np.mean(np.log1p(signal)[:, np.newaxis].reshape((len(signal)/genome_bin, genome_bin)), axis=1))
  np.random.shuffle(lsmeans) #random bins
  #here add padding, in case
  notediff = (np.log1p(np.median(ss.max_val)) -  np.log1p(1)) / (12 * _mapped_octaves)
  octaves = build_octave(notediff, int(_mapped_octaves * 1.5))
  lnotes  = np.sum(lsmeans > octaves[:, np.newaxis], axis=0) - 1 
  

  if scale == 'major':
    lnotes = map_scale(lnotes, np.array([0, 0, 2, 2, 4, 5, 5, 7, 7, 9, 9, 11]))
  elif scale == 'minor':
    lnotes = map_scale(lnotes, np.array([0, 0, 2, 3, 3, 5, 5, 7, 8, 8, 10, 10]))  
  elif scale == 'blues':
    lnotes = map_scale(lnotes, np.array([0, 0, 0, 3, 3, 5, 6, 7, 8, 9, 10, 10]))  
  elif scale == 'flamenco':
    lnotes = map_scale(lnotes, np.array([0, 1, 1, 3, 3, 5, 6, 6, 8, 9, 10, 10]))  
  elif scale == 'phrygian':
    lnotes = map_scale(lnotes, np.array([0, 1, 1, 3, 3, 5, 5, 7, 8, 8, 10, 10]))  
  elif scale == 'harmonic_minor':
    lnotes = map_scale(lnotes, np.array([0, 0, 2, 3, 3, 5, 5, 7, 8, 8, 8, 11]))  
  elif scale == 'minor_pentatonic':
    lnotes = map_scale(lnotes, np.array([0, 0, 0, 3, 3, 5, 5, 7, 7, 7, 10, 10]))  
  elif scale == 'major_pentatonic':
    lnotes = map_scale(lnotes, np.array([0, 0, 2, 2, 4, 4, 4, 7, 7, 9, 9, 9]))  

  if velocity_file_name:
    vh = bigwig.BigWigFile(open(velocity_file_name, 'rb'))
    v_signal = vh.get_as_array(chrom, start, end)
    v_signal[np.isnan(v_signal)] = 0
    v_signal = np.append(v_signal, np.zeros(genome_bin - (len(v_signal) % genome_bin)))
    vlmeans = np.log1p(np.mean(np.log1p(v_signal)[:, np.newaxis].reshape((len(v_signal)/genome_bin, genome_bin)), axis=1))
    vdiff =  np.ptp(np.log1p(v_signal))  / 8
    v_temp = np.sum(vlmeans > np.cumsum(np.ones(8) * vdiff)[:, np.newaxis], axis=0) 
    velocity_data = 8 * (v_temp[1:] - v_temp[:-1]) + 96
    velocity_data[velocity_data > 127] = 127    
    velocity_data[velocity_data < 0] = 0
    velocity_data = np.append(np.zeros(1, dtype=int), velocity_data)
  else:
    velocity_data = np.zeros(len(lnotes), dtype=int) + 96   



  midi_data = []
  last_note = lnotes[0]
  note_length = 1
  for x in range(1, len(lnotes)):
    this_note = lnotes[x]
    this_vel = velocity_data[x]
    if this_note == last_note:
      note_length += 1
    else:
      midi_data.append([note_length * tick_size, this_vel, last_note])
      last_note = this_note
      note_length = 1

  track = midi.Track()

  tick = 0
  for x in midi_data:
    if x[2] > -1:
      on = midi.NoteOnEvent(tick = tick, velocity = x[1], pitch = x[2] + tune)
      track.append(on)
      tick = 0
      off = midi.NoteOffEvent(tick = tick + x[0], pitch = x[2] + tune)
      track.append(off)
    else:
      tick = x[0]

  track.append(midi.EndOfTrackEvent(tick=1))
  return track

def region_from_string(s):
  t = s.split(':')
  chrom = t[0]
  [start, end] = [int(x) for x in t[1].split('-')]
  return [chrom, start, end]

def read_bed_file(file_name):
  regions = []
  for line in open(file_name):
    if line.startswith('#'):
      continue
    t = line.strip().split()
    t[1] = int(t[1])
    t[2] = int(t[2])
    regions.append(t)
  return regions

def build_single_MIDI_pattern():

  note_list = ['C', 'Cs', 'D', 'Ds', 'E', 'F', 'Fs', 'G', 'Gs', 'A', 'As', 'B']
  regions_play = []

  # parse options
  option_parser = argparse.ArgumentParser(
  description="Create a single-pattern MIDI file from a BigWig file",
  prog="MIDI_random.py",
  epilog="For any question, write to cittaro.davide@hsr.it")
  option_parser.add_argument("--version", action="version", version="%(prog)s 0.1")
  option_parser.add_argument("-i", "--input_data", help="BigWig file containing data that needs to be played", action='store', type=str, required=True)
  option_parser.add_argument("-v", "--velocity_data", help="BigWig file containing data that will be used to tune colors (volume and accents)", action='store', type=str)
  option_parser.add_argument("-r", "--region", help="Genomic region that will be converted to MIDI file (chr:start-end)", action='store')
  option_parser.add_argument("-b", "--bed", help="BED file containing multiple regions that will be converted to MIDI file.", action='store')
  option_parser.add_argument("-s", "--scale", help="The scale to be used", action='store', type=str, choices = ['chromatic', 'major', 'minor', 'major_pentatonic', 'minor_pentatonic', 'harmonic_minor', 'blues', 'phrygian'], default = 'chromatic' )
  option_parser.add_argument("-O", "--octave", help="The octave to be used", action='store', type=int, default=4)
  option_parser.add_argument("-k", "--key", help="The key note for the track", action='store', type=str, default='C', choices = note_list)
  option_parser.add_argument("-o", "--output", help="Basename of output MIDI file", action='store', default='pattern')
# Right now I will present two options for note length. One day I will manage to set track tempo and modify genome bins and tick size accordingly.
  option_parser.add_argument("-g", "--genome_bin", help="The size of genomic bins that will be used to sample data. Ideally this is linked to the length of a note", action='store', type=int, default=200)
  option_parser.add_argument("-t", "--tick_size", help="The size of MIDI tick. Ideally this is linked to the length of a note (at default resolution, 480 = 1/8)", action='store', type=int, default=480)

  options = option_parser.parse_args()
  

  
  # do some processing
  if not options.bed and not options.region:
    sys.stderr.write("Yo! You'll need some region to play. I cannot play the whole genome (not yet!)\n")
    sys.exit(1)
  if options.bed:  
    regions_play = read_bed_file(options.bed)
  if options.region:  
    regions_play.append(region_from_string(options.region))
  
  tune = 12 * options.octave + note_list.index(options.key)

  for region in regions_play:
    # loop over defined regions and create a separate file for each.
    # yes, right now we will handle those like this    
    
    if region[1] < 0:
      sys.stderr.write("Yo! You want to play negative regions? Skipping this\n")
      continue
    
    out_file = "%s_%s-%d-%d_%s-%s%d_t%s-g%s.mid" % (options.output, region[0], region[1], region[2], options.scale, options.key, options.octave, options.tick_size, options.genome_bin )
  
    #instantiate a new pattern. Once I will read all docs about, I know I will find useful things to add here
    MIDI_pattern = midi.Pattern(resolution = _resolution)
    
    #build track from data
    MIDI_track = build_track(options.input_data, region[0], region[1], region[2], options.scale, tune, options.tick_size, options.genome_bin, options.velocity_data)
    
    MIDI_pattern.append(MIDI_track)
    
    midi.write_midifile(out_file, MIDI_pattern)


if __name__ == '__main__':
  build_single_MIDI_pattern()


